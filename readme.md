http://ceko-bbnetwork.rhcloud.com/

## Openshift repo

`git clone ssh://58cb01cc2d5271dc3b00004e@ceko-bbnetwork.rhcloud.com/~/git/ceko.git/`

## Bind Openshift repo into bitbucket repo

`git remote add openshirft -f ssh://58cb01cc2d5271dc3b00004e@ceko-bbnetwork.rhcloud.com/~/git/ceko.git/`

`git merge openshift/master -s recursive -X ours`

push to remote repo:

```
git push openshift HEAD
```

## RHC

#### Install
https://developers.openshift.com/managing-your-applications/client-tools.html

`$ rhc app-restart ceko`

Force restart

`$ rhc app-force-stop <app_name>`
`$ rhc app-start <app_name>`

#### Logs
https://developers.openshift.com/managing-your-applications/log-files.html

`$ rhc tail -a ceko`

#### TODO

response,
local config,
andy mail,
ceko receiver