var express = require('express');
var nodemailer = require('nodemailer');
var bodyParser = require('body-parser');

var config = require('./config');
var localConfig = require('./config.local') || null;

if(localConfig){
  Object.assign(config, localConfig);
}

var app = express();

app.use( bodyParser.json() );

//app.listen(config.port);
var ipaddress = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

app.listen(port, ipaddress, function() {
  // Do your stuff
});

// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.post('/sendmail/ceko', function(req, res) {

  console.log(req.body);

  var smtpConfig = {
    host: config.ceko.host,
    port: 465,
    secure: true, // use SSL
    auth: {
      user: config.ceko.mail,
      pass: config.ceko.password
    }
  };

  var transporter = getTransporter(smtpConfig);

  var mailOptions = {
    from: '"Formular na stranke CeKo" <obchod@ceko.sk>', // sender address
    to: config.ceko.mail, // list of receivers
    subject: 'CeKo', // Subject line
    text: 'name: ' + req.body.name  + ' mail: ' + req.body.mail + ' text: ' + req.body.text, // plaintext body
    html: 'Meno: <b>' + req.body.name + '</b><br/>' +
    'E-mail: ' + req.body.mail + '<br/><br/>' + req.body.text
  };

  sendMail(transporter, mailOptions, res);

});

var getTransporter = function(smtpConfig){
  return nodemailer.createTransport(smtpConfig);
}

var sendMail = function(transporter, mailOptions, res) {
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      res.status(500).end(error);
      return console.log(error);
    }
    console.log('Message sent: ' + info.response);
    return res.status(200).end('Message sent: ' + info.response);
  });
}